﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship[] enemySpaceship;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        private GameObject[] remainingEnemy;
        private int enemyCount;

        [HideInInspector]
        public PlayerSpaceship spawnedPlayerShip;

        public event Action OnRestarted;

        public static GameManager Instance { get; private set; }

        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

        }

        public void StartGame()
        {
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            foreach (var enemy in enemySpaceship)
            {
                Vector2 position = new Vector2(UnityEngine.Random.Range(-10, 10), UnityEngine.Random.Range(2, 5));
                var spawnedEnemyShip = Instantiate(enemy, position, Quaternion.identity);
                spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
                spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
            }

            remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            enemyCount = remainingEnemy.Length;
            // Debug.Log(enemyCount);
        }

        private void OnEnemySpaceshipExploded()
        {
            // ScoreManager.Instance.UpdateScore(10);

            enemyCount -= 1;
            EnemyCheck();
            // Restart();
            // SceneManager.LoadScene("Game2");
        }

        void EnemyCheck()
        {
            if (enemyCount <= 0)
            {
                Restart();
            }
        }

        private void Restart()
        {
            OnRestarted?.Invoke();
            DestroyRemainingShips();
        }

        // private void LoadNextScene()
        // {
        //     SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        // }

        private void DestroyRemainingShips()
        {
            var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemy)
            {
                Destroy(enemy);
            }

            var remainingBoss = GameObject.FindGameObjectsWithTag("Boss");
            foreach (var boss in remainingBoss)
            {
                Destroy(boss);
            }

            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }
    }
}
