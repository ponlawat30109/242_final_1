using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BackToMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timerText;
    private float timeleft = 5f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeleft -= Time.deltaTime;
        timerText.text = $"Return to Mainmenu in {timeleft:0} second";
        if(timeleft <= 0)
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
