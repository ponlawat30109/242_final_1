using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScripts : MonoBehaviour
{
    [SerializeField] Button startButton;

    void Start()
    {
        startButton.onClick.AddListener(OnStartButton);
    }

    void OnStartButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
}
